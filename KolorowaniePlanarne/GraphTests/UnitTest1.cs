﻿using System;
using System.Linq;
using KolorowaniePlanarne.GrapColouring;
using KolorowaniePlanarne.Graphs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GraphTests
{
	[TestClass]
	public class UnitTest1
	{
		private void ConfirmColouring(IGraph graph, GraphPaintingResult coloring)
		{
			Assert.IsTrue(coloring.Done);

			for (int i = 0; i < graph.VerticesCount; i++)
			{
				foreach (var neighbour in graph.GetEdgesFrom(i))
				{
					Assert.AreNotEqual(coloring.Colours[i], coloring.Colours[neighbour]);
				}
			}

			int max = coloring.Colours.Max();
			int distincts = coloring.Colours.Distinct().Count();
			Assert.AreEqual(max, distincts);
			Assert.IsFalse(coloring.Colours.Contains(0));

		}

		[TestMethod]
		public void TestMethod1()
		{
			var graph = GraphTestCases.TestCase1();
			var result = GraphPainter.PaintGraph(graph);

			Assert.IsTrue(result.Done);

			ConfirmColouring(graph, result);


		}

		[TestMethod]
		public void TestMethod2()
		{
			var graph = GraphTestCases.TestCase2();
			var result = GraphPainter.PaintGraph(graph);

			Assert.IsFalse(result.Done);


		}

		[TestMethod]
		public void TestMethod3()
		{
			var graph = GraphTestCases.TestCase3();
			var result = GraphPainter.PaintGraph(graph);

			Assert.IsTrue(result.Done);
			ConfirmColouring(graph, result);


		}

		[TestMethod]
		public void TestMethod4()
		{
			var graph = GraphTestCases.TestCase4();
			var result = GraphPainter.PaintGraph(graph);

			Assert.IsFalse(result.Done);
		}

		[TestMethod]
		public void TestMethod5()
		{
			var graph = GraphTestCases.TestCase5();
			var result = GraphPainter.PaintGraph(graph);

			Assert.IsTrue(result.Done);
			ConfirmColouring(graph, result);


		}

		[TestMethod]
		public void TestMethod6()
		{
			var graph = GraphTestCases.TestCase6();
			var result = GraphPainter.PaintGraph(graph);

			Assert.IsTrue(result.Done);
			ConfirmColouring(graph, result);


		}
	}
}
