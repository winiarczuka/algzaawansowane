﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KolorowaniePlanarne.GrapColouring;
using KolorowaniePlanarne.Graphs;
using Microsoft.Win32;

namespace KolorowaniePlanarne
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		IGraph loadedGraph;
		GraphPaintingResult result;
		//MainViewModel model;
		public MainWindow()
		{
			//model = new MainViewModel();
			//DataContext = model;
			InitializeComponent();


		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{

			OpenFileDialog dialog = new OpenFileDialog();
			if (dialog.ShowDialog() == true)
			{
				loadedGraph = GraphFactory.LoadMatrixGraph(dialog.FileName);
				if (loadedGraph == null)
				{
					ResultText.Text = "Couldn't load graph. Check if file is valid";
					return;
				}
				result = GraphPainter.PaintGraph(loadedGraph);
				if (result.Done)
				{
					ResultText.Text = "Graph painted";
					var res = GraphFactory.SaveColouring(result);
					Result.Text = res.ToString();
				}
				else
					ResultText.Text = "Graph could not be painted";

			}
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			if (!result.Done)
				return;
			SaveFileDialog dialog = new SaveFileDialog();
			if (dialog.ShowDialog() == true)
			{
				var res = GraphFactory.SaveColouring(result);
				try
				{
					File.WriteAllText(dialog.FileName, res.ToString());
					ResultText.Text = "File saved";

				}
				catch (Exception ex)
				{
					ResultText.Text = "File couldn't be saved";
					
				}
			}
		}

		private void Button_Click_2(object sender, RoutedEventArgs e)
		{
			GraphFactory.SaveGraphToFile("testCase1.txt.", GraphTestCases.TestCase1());
			GraphFactory.SaveGraphToFile("testCase2.txt.", GraphTestCases.TestCase2());
			GraphFactory.SaveGraphToFile("testCase3.txt.", GraphTestCases.TestCase3());
			GraphFactory.SaveGraphToFile("testCase4.txt.", GraphTestCases.TestCase4());
			GraphFactory.SaveGraphToFile("testCase5.txt.", GraphTestCases.TestCase5());
			GraphFactory.SaveGraphToFile("testCase6.txt.", GraphTestCases.TestCase6());
		}
	}
}
