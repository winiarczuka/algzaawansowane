﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolorowaniePlanarne.Graphs
{
	public interface IGraph
	{

		int VerticesCount { get; }
		bool EdgeExists(int from, int to);
		int AddVertex();
		void AddEdge(int from, int to);
		void RemoveEdge(int from, int to);
		IGraph Clone();
		IEnumerable<int> GetEdgesFrom(int vertex);
		int Neighbours(int vertex);
	}
}
