﻿using KolorowaniePlanarne.GrapColouring;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolorowaniePlanarne.Graphs
{
	public static class GraphFactory
	{
		public static IGraph CreateMatrixGraph(int size)
		{
			return new MatrixGraph(size);
		}

		public static void SaveGraphToFile(string filename, IGraph graph)
		{
			var file = new StreamWriter(filename);

			file.WriteLine(graph.VerticesCount);
			for(int i=0;i<graph.VerticesCount;i++)
			{
				int n = graph.GetEdgesFrom(i).Where(x=>x>i).Count();
				if (n <= 0)
					continue;
				file.Write(i + 1 + " :");
				foreach (var edge in graph.GetEdgesFrom(i).Where(x=>x>i))
				{
					file.Write(" " + (edge+1));
				}
				file.WriteLine();
			}


			file.Close();
		}

		public static IGraph LoadMatrixGraph(string fileName)
		{
			List<string> content = File.ReadLines(fileName).ToList();
			try
			{
				int count = int.Parse(content[0]);
				var graph = CreateMatrixGraph(count);
				foreach (var line in content.Skip(1))
				{
					var parts = line.Split(':');

					int vertex = int.Parse(parts[0]) - 1;
					var vertices = parts[1].Split(' ');
					foreach (var vert in vertices.Where(x => x.Length > 0))
					{
						graph.AddEdge(vertex, int.Parse(vert) - 1);
					}
				}
				return graph;
			}
			catch
			{
				return null;
			}
		}

		public static StringWriter SaveColouring(GraphPaintingResult result)
		{
			StringWriter file = new StringWriter();
			var maxColor = result.Colours.Max();
			List<int>[] verticesColours = new List<int>[maxColor];
			for (int i = 0; i < maxColor; i++)
				verticesColours[i] = new List<int>();
			for (int i = 0; i < result.Colours.Length; i++)
				verticesColours[result.Colours[i] - 1].Add(i + 1);

			for (int i = 0; i < maxColor; i++)
			{
				file.Write((i + 1) + " :");
				foreach (var vert in verticesColours[i])
				{
					file.Write(" " + vert);
				}
				file.WriteLine();
			}
			file.Close();
			return file;
		}
	}
}
