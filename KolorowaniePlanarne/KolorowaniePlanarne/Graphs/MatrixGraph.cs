﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolorowaniePlanarne.Graphs
{
	class MatrixGraph : IGraph
	{
		private bool[,] matrix;
		private int[] verticesDegree;
		internal MatrixGraph(int size)
		{
			matrix = new bool[size, size];
			verticesDegree = new int[size];
		}

		private MatrixGraph(MatrixGraph graph)
		{
			matrix = new bool[graph.VerticesCount, graph.VerticesCount];
			verticesDegree = new int[graph.VerticesCount];
			graph.verticesDegree.CopyTo(verticesDegree, 0);
			matrix = graph.matrix.Clone() as bool[,];
		}

		public int VerticesCount
		{
			get
			{
				return matrix.GetLength(0);
			}
		}

		public void AddEdge(int from, int to)
		{
			matrix[from, to] = true;
			matrix[to, from] = true;
			verticesDegree[from] += 1;
			verticesDegree[to] += 1;
		}

		public void RemoveEdge(int from, int to)
		{
			matrix[from, to] = false;
			matrix[to, from] = false;
			verticesDegree[from] -= 1;
			verticesDegree[to] -= 1;
		}

		public int AddVertex()
		{
			var tmp = matrix;
			matrix = new bool[matrix.GetLength(0) + 1, matrix.GetLength(0) + 1];
			for (int i = 0; i < tmp.GetLength(0); i++)
				for (int j = 0; j < tmp.GetLength(1); j++)
					matrix[i, j] = tmp[i, j];
			var temp = new int[verticesDegree.Length + 1];
			verticesDegree.CopyTo(temp, 0);
			verticesDegree = temp;
			return tmp.GetLength(0);
		}

		public bool EdgeExists(int from, int to)
		{
			return matrix[from, to];
		}

		public IEnumerable<int> GetEdgesFrom(int vertex)
		{
			for(int i=0;i<matrix.GetLength(0);i++)
			{
				if (matrix[vertex, i])
					yield return i;
			}
			yield break;
		}

		public int Neighbours(int vertex)
		{
			int neighbours = 0;
			for (int i = 0; i < matrix.GetLength(0); i++)
			{
				if (matrix[vertex, i])
					neighbours += 1;
			}
			return neighbours;
		}

		public IGraph Clone()
		{
			return new MatrixGraph(this);
		}
	}
}
