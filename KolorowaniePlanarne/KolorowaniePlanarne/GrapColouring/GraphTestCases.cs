﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using KolorowaniePlanarne.Graphs;

namespace KolorowaniePlanarne.GrapColouring
{
	public static class GraphTestCases
	{
		public static IGraph TestCase1()
		{
			var graph = GraphFactory.CreateMatrixGraph(10);
			return graph;
		}

		//graf pełny, 10 wierzchołków.
		public static IGraph TestCase2()
		{
			var graph = GraphFactory.CreateMatrixGraph(10);
			for (int i = 0; i < graph.VerticesCount; i++)
				for (int j = i + 1; j < graph.VerticesCount; j++)
					graph.AddEdge(i, j);
			return graph;
		}

		// Graf K5 z wystającym wierzchołkiem połączonym jedną krawędzią
		public static IGraph TestCase3()
		{
			var graph = GraphFactory.CreateMatrixGraph(6);
			for (int i = 0; i < 5; i++)
				for (int j = i + 1; j < 5; j++)
					graph.AddEdge(i, j);
			graph.AddEdge(0, 5);
			return graph;
		}

		// graf pełny, 6 wierzchołków
		public static IGraph TestCase4()
		{
			var graph = GraphFactory.CreateMatrixGraph(6);
			for (int i = 0; i < 6; i++)
				for (int j = i + 1; j < 6; j++)
					graph.AddEdge(i, j);

			return graph;
		}

		//graf k3,3
		public static IGraph TestCase5()
		{
			var graph = GraphFactory.CreateMatrixGraph(6);
			graph.AddEdge(0, 3);
			graph.AddEdge(0, 4);
			graph.AddEdge(0, 5);
			graph.AddEdge(1, 3);
			graph.AddEdge(1, 4);
			graph.AddEdge(1, 5);
			graph.AddEdge(2, 3);
			graph.AddEdge(2, 4);
			graph.AddEdge(2, 5);
			return graph;
		}

		//5regular planar graph
		public static IGraph TestCase6()
		{
			var graph = GraphFactory.CreateMatrixGraph(12);

			//outer triangle
			graph.AddEdge(0, 1);
			graph.AddEdge(1, 2);
			graph.AddEdge(2, 0);

			graph.AddEdge(0, 3);
			graph.AddEdge(1, 3);
			graph.AddEdge(1, 4);
			graph.AddEdge(2, 4);
			graph.AddEdge(2, 5);
			graph.AddEdge(0, 5);

			graph.AddEdge(5, 6);
			graph.AddEdge(0, 6);
			graph.AddEdge(3, 6);

			graph.AddEdge(1, 7);
			graph.AddEdge(3, 7);
			graph.AddEdge(4, 7);

			graph.AddEdge(4, 8);
			graph.AddEdge(2, 8);
			graph.AddEdge(5, 8);

			graph.AddEdge(9,6);
			graph.AddEdge(9,3);
			graph.AddEdge(9,7);
			graph.AddEdge(9,10);
			graph.AddEdge(9,11);

			graph.AddEdge(10,7);
			graph.AddEdge(10,4);
			graph.AddEdge(10,8);
			graph.AddEdge(10,11);

			graph.AddEdge(11,8);
			graph.AddEdge(11,5);
			graph.AddEdge(11,6);

			return graph;
		}

		public static IGraph TestCase7()
		{
			var graph = GraphFactory.CreateMatrixGraph(10);
			for (int i = 0; i < 6; i++)
				for (int j = i + 1; j < 6; j++)
					graph.AddEdge(i, j);

			return graph;
		}
	}
}
