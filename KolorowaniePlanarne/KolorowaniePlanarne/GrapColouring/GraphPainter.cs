﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Animation;

namespace KolorowaniePlanarne.GrapColouring
{
	public struct GraphPaintingResult
	{
		public bool Done;
		public int[] Colours;
	}



	public static class GraphPainter
	{
		private interface IVertex
		{
			IEnumerable<int> GetVertices();
			int ID { get; }
		}

		private class StandardVertex : IVertex
		{
			private int vertexID;

			public int ID => vertexID;

			internal StandardVertex(int vertex)
			{
				vertexID = vertex;
			}

			public IEnumerable<int> GetVertices()
			{
				yield return vertexID;
			}

		}

		private class CompoundVertex : IVertex
		{
			private IVertex first;
			private IVertex second;
			private int id;
			internal CompoundVertex(int id, IVertex one, IVertex two)
			{
				first = one;
				second = two;
				this.id = id;
			}

			public int ID => id;

			public IEnumerable<int> GetVertices()
			{
				foreach (var vert in first.GetVertices().Concat(second.GetVertices()))
				{
					yield return vert;
				}
			}
		}

		private class UncolouredVertices
		{
			public int Id { get; set; }
			public int Neighbours { get; set; }

			public void Decrease()
			{
				this.Neighbours--;
			}
		}
		public static GraphPaintingResult PaintGraph(Graphs.IGraph graph)
		{
			GraphPaintingResult result;
			result.Done = false;
			//wynikowe kolory. Będzie ich tyle co wierzchołków wejściowego grafu.
			result.Colours = new int[graph.VerticesCount];

			// stos na który odkładane będą kolejne wierzchołki
			Stack<IVertex> verticesStack = new Stack<IVertex>();

			var g = graph.Clone();

			List<IVertex> verticesToColor = new List<IVertex>();
			List<UncolouredVertices> uncolouredVerticeses = new List<UncolouredVertices>();



			for (int i = 0; i < g.VerticesCount; i++)
			{
				uncolouredVerticeses.Add(new UncolouredVertices() { Id = i, Neighbours = g.Neighbours(i) });
				verticesToColor.Add(new StandardVertex(i));
			}


			while (uncolouredVerticeses.Count > 0)
			{
				var min = int.MaxValue;
				UncolouredVertices vert = null;
				foreach (var uncolouredVerticese in uncolouredVerticeses)
				{
					if (uncolouredVerticese.Neighbours < min)
					{
						min = uncolouredVerticese.Neighbours;
						vert = uncolouredVerticese;
					}
				}

				Debug.Assert(vert != null);

				if (vert.Neighbours < 5)
				{
					//easy part

					foreach (var edge in g.GetEdgesFrom(vert.Id))
					{
						uncolouredVerticeses.FirstOrDefault(x => x.Id == edge)?.Decrease();
					}

					var vertex = verticesToColor.FirstOrDefault(x => x.ID == vert.Id);

					Debug.Assert(vertex != null);

					verticesStack.Push(vertex);
					verticesToColor.Remove(vertex);
					uncolouredVerticeses.Remove(vert);

				}
				else
				{
					if (vert.Neighbours > 5)//non planar graph
					{
						result.Colours = null;
						return result;
					}
					//hard part
					var n = g.GetEdgesFrom(vert.Id).ToList();

					var neighbours = uncolouredVerticeses.Where(x => n.Contains(x.Id)).ToList();
					var realVerts = verticesToColor.Where(x => n.Contains(x.ID)).ToList();
					if (neighbours.Count != vert.Neighbours || realVerts.Count != vert.Neighbours)
						throw new Exception("Bad vert count!!");

					//K5 check
					int one = -1;
					int two = -1;
					bool k5 = true;
					for (int i = 0; i < 5; i++)
						for (int j = i + 1; j < 5; j++)
						{
							if (!g.EdgeExists(neighbours[i].Id,neighbours[j].Id))
							{
								k5 = false;
								one = i;
								two = j;
							}
						}
					if (k5)
					{
						result.Colours = null;
						return result;
					}

					var firstNeigh = realVerts[one];
					var frst = neighbours[one];
					var scnd = neighbours[two];
					var secondNeigh = realVerts[two];

					uncolouredVerticeses.Remove(frst);
					uncolouredVerticeses.Remove(scnd);
					verticesToColor.Remove(firstNeigh);
					verticesToColor.Remove(secondNeigh);

					var neigh = g.GetEdgesFrom(frst.Id).Concat(g.GetEdgesFrom(scnd.Id)).ToList();
					var newNeigh = verticesToColor.Where(x => neigh.Contains(x.ID)).ToList();

					var newId = g.AddVertex();
					foreach (var neighbour in newNeigh)
					{
						g.AddEdge(newId, neighbour.ID);
					}
					g.AddEdge(vert.Id,newId);
					var newVert = new CompoundVertex(newId, firstNeigh, secondNeigh);
					verticesToColor.Add(newVert);
					uncolouredVerticeses.Add(new UncolouredVertices() {Id = newId, Neighbours = newNeigh.Count});
					var vertex = verticesToColor.FirstOrDefault(x => x.ID == vert.Id);

					Debug.Assert(vertex != null);

					verticesStack.Push(vertex);
					verticesToColor.Remove(vertex);
					uncolouredVerticeses.Remove(vert);

					foreach (var edge in g.GetEdgesFrom(vert.Id))
					{
						uncolouredVerticeses.FirstOrDefault(x => x.Id == edge)?.Decrease();
					}
				}



			}

			//Listy kolorów użytych w sąsiadach danego wierzchołka. Może ich być więcej niż w wejściowym grafie bo graf g może mieć dodatkowe wierzchołki

			//TODO : zamienić listy na słowniki.
			List<int>[] colorsUsed = new List<int>[g.VerticesCount];
			for (int i = 0; i < g.VerticesCount; i++)
				colorsUsed[i] = new List<int>(); // inicjujemy pustymi listami

			while (verticesStack.Count > 0)
			{
				//bierzemy wierzchołek z wierzchu stosu
				var vert = verticesStack.Pop();
				//tworzymy listę użytych kolorów
				var colors = colorsUsed[vert.ID];

				int minColor = 1;
				while (minColor < 6 && colors.Contains(minColor))
					minColor++;


				if (minColor == 0 || minColor > 5)
				{
					result.Colours = null;
					return result;
				}
				var colorToUse = minColor;

				// zwykłe wierzchołki zwróca tylko jeden indeks. Wierzchołki składające się z wielu (2, a jeśli wielokrotnie łączone to nawet więcej)
				// na raz pokolorują wszystkie z nich na wybrany kolor.
				// Nie ma potrzeby rozważać indeksów wierzchołków łączonych, bo wszyscy ich sąsiedzi mają jakiegoś sąsiada będącego częścią tego wierzcholka

				foreach (var vertex in vert.GetVertices())
				{
					result.Colours[vertex] = colorToUse;
					foreach (var neigh in g.GetEdgesFrom(vertex))
						colorsUsed[neigh].Add(colorToUse);
				}
			}
			result.Done = true;
			return result;
		}
	}
}
